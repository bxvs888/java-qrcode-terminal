package com.vvvtimes.util;

import java.io.Console;
import java.util.Hashtable;
import java.util.Scanner;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class Main {

	private static boolean isIDE = false;

	private static boolean isWindows = false;

	private static int IDE = 0;

	public static void main(String[] args) throws Exception {
		if (isWindows) {
			if (isIDE) {
				System.out
						.println("当前环境设置为windows+eclipse,eclipse为反色显示，需要在控制台选中");
				Scanner input = new Scanner(System.in);
				String val = null; // 记录输入的字符串
				boolean isvalid = false;
				do {
					System.out.println("请选择IDE：0 eclipse 1 idea 2 netbeans");
					val = input.nextLine(); // 等待输入值
					try {
						// 把字符串强制转换为数字
						int num = Integer.valueOf(val);
						// 如果是数字，返回True
						if (num >= 0 && num <= 2) {
							IDE = num;
							isvalid = true;
						} else {
							isvalid = false;
						}
					} catch (Exception e) {
						// 如果抛出异常，返回False
						isvalid = false;
					}
					if (!isvalid) {
						System.out.println("输入有误，请重新输入");
					}

				} while (!isvalid); // 如果输入的值不在范围就继续输入
				System.out.println("您选择了" + IDE + "，进入处理程序");
				input.close(); // 关闭资源
			} else {
				System.out
						.println("当前为windows非IDE环境，若显示不正常请先执行如下命令后回车!\nchcp 437 & mode 200, 200");
			}
		}

		String text = "https://gitee.com/gsls200808/java-qrcode-terminal";
		System.out.println(getQr(text));

	}

	public static String getQr(String text) {
		String s = "生成二维码失败";
		int width = 40;
		int height = 40;
		// 用于设置QR二维码参数
		Hashtable<EncodeHintType, Object> qrParam = new Hashtable<EncodeHintType, Object>();
		// 设置QR二维码的纠错级别——这里选择最低L级别
		qrParam.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		qrParam.put(EncodeHintType.CHARACTER_SET, "utf-8");
		try {
			BitMatrix bitMatrix = new MultiFormatWriter().encode(text,
					BarcodeFormat.QR_CODE, width, height, qrParam);
			s = toAscii(bitMatrix);
		} catch (WriterException e) {
			e.printStackTrace();
		}
		return s;
	}

	public static String toAscii(BitMatrix bitMatrix) {
		StringBuilder sb = new StringBuilder();
		for (int rows = 0; rows < bitMatrix.getHeight(); rows++) {
			for (int cols = 0; cols < bitMatrix.getWidth(); cols++) {
				boolean x = bitMatrix.get(rows, cols);

				if (isWindows) {
					if (isIDE) {
						switch (IDE) {
						case 0:
							if (!x) {// windows+eclipse适用
								// white
								sb.append("██");
							} else {
								sb.append("  ");
							}
							break;

						case 1:
							if (!x) {// windows+idea适用
								// white
								sb.append("\033[47m  \033[0m");
							} else {
								sb.append("\033[40m  \033[0m");
							}
							break;

						case 2:
							if (!x) {// windows+NetBeans适用
								// white
								sb.append("██");
							} else {
								sb.append("  ");
							}
							break;

						default:
							if (!x) {// windows+其它IDE适用
								// white
								sb.append("██");
							} else {
								sb.append("  ");
							}
							break;

						}

					} else {// windows非IDE环境适用 chcp 437 & mode 200, 200
						if (!x) {
							// white
							sb.append("█");
						} else {
							sb.append(" ");
						}
					}

				} else {// linux适用
					if (!x) {
						// white
						sb.append("\033[47m  \033[0m");
					} else {
						sb.append("\033[40m  \033[0m");
					}
				}
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	static {
		Console console = System.console();
		if (console == null) {
			isIDE = true;
		}

		String os = System.getProperty("os.name");
		if (os.toLowerCase().startsWith("win")) {
			isWindows = true;
		}
	}

}